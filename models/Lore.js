var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Lore Model
 * ==========
 */

var Lore = new keystone.List('Lore', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true }
});

Lore.add({
	title: { type: String, required: true },
	state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
	author: { type: Types.Relationship, ref: 'User', index: true },
	publishedDate: { type: Types.Date, index: true, dependsOn: { state: 'published' } },
	image: { type: Types.LocalFile,
					 dest: '/public/images/lore',
					 allowedTypes: ['image/jpeg', 'image/png'],
					 filename: function(Lore, image) {
						 return Lore.id + '.' + image.extension
					 }
					},
	content: {
		brief: { type: Types.Html, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 }
	},
	races: { type: Types.Relationship, ref: 'LoreRace', many: true }
});

Lore.schema.virtual('content.full').get(function() {
	return this.content.extended || this.content.brief;
});

Lore.defaultColumns = 'title, state|20%, author|20%, publishedDate|20%';
Lore.register();
