var keystone = require('keystone');

/**
 * LoreRace Model
 * ==================
 */

var LoreRace = new keystone.List('LoreRace', {
	autokey: { from: 'name', path: 'key', unique: true }
});

LoreRace.add({
	name: { type: String, required: true }
});

LoreRace.relationship({ ref: 'Lore', path: 'races' });

LoreRace.register();
