var keystone = require('keystone');
var async = require('async');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = 'codex';
	locals.filters = {
		race: req.params.race
	};
	locals.data = {
		lores: [],
		races: []
	};

	// Load all categories
	view.on('init', function(next) {

		keystone.list('LoreRace').model.find().sort('name').exec(function(err, results) {

			if (err || !results.length) {
				return next(err);
			}

			locals.data.races = results;

			// Load the counts for each race
			async.each(locals.data.races, function(race, next) {

				keystone.list('Lore').model.count().where('races').in([race.id]).exec(function(err, count) {
					race.loreCount = count;
					next(err);
				});

			}, function(err) {
				next(err);
			});

		});

	});

	// Load the current race filter
	view.on('init', function(next) {

		if (req.params.race) {
			keystone.list('LoreRace').model.findOne({ key: locals.filters.race }).exec(function(err, result) {
				locals.data.race = result;
				next(err);
			});
		} else {
			next();
		}

	});

	// Load the posts
	view.on('init', function(next) {

		var q = keystone.list('Lore').paginate({
				page: req.query.page || 1,
				perPage: 10,
				maxPages: 10
			})
			.where('state', 'published')
			.sort('-publishedDate')
			.populate('author races');

		if (locals.data.race) {
			q.where('races').in([locals.data.race]);
		}

		q.exec(function(err, results) {
			locals.data.lores = results;
			next(err);
		});

	});

	// Render the view
	view.render('codex');

};
