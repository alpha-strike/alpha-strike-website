var keystone = require('keystone');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'codex';
	locals.filters = {
		lore: req.params.lore
	};
	locals.data = {
		lores: []
	};

	// Load the current post
	view.on('init', function(next) {

		var q = keystone.list('Lore').model.findOne({
			state: 'published',
			slug: locals.filters.lore
		}).populate('author races');

		q.exec(function(err, result) {
			locals.data.lore = result;
			next(err);
		});

	});

	// Load other posts
	view.on('init', function(next) {

		var q = keystone.list('Lore').model.find().where('state', 'published').sort('-publishedDate').populate('author').limit('4');

		q.exec(function(err, results) {
			locals.data.lores = results;
			next(err);
		});

	});

	// Render the view
	view.render('lore');

};
