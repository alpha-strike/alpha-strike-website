#!/bin/bash

apt-add-repository ppa:ubuntu-toolchain-r/test
wait
apt-get update && apt-get upgrade -y
wait
curl -sL https://deb.nodesource.com/setup_0.12 | sudo -E bash -
wait
apt-get install nginx nodejs mongodb unicode -y
wait
cd /var/www/alpha-strike-website
wait
npm install forever -g
wait
forever start keystone.js

